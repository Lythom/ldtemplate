package world;
import com.haxepunk.gui.MenuItem;
import com.haxepunk.gui.MenuList;
import com.haxepunk.Scene;

class TestWorld extends Scene
{
	public function new()
	{
		super();
	}

	override public function begin()
	{
		var menu_start = new MenuItem("Start Game");
		var menu_scores = new MenuItem("Scores");
		var menu_about = new MenuItem("About");

		var menu = new MenuList(100, 100);
		menu.addControl(menu_start);
		menu.addControl(menu_scores);
		menu.addControl(menu_about);

		add(menu);
	}

	override public function end()
	{
		removeAll();
		super.end();
	}

}