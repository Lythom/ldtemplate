package world;

import com.haxepunk.Entity;
import com.haxepunk.Graphic.ImageType;
import com.haxepunk.graphics.Graphiclist;
import com.haxepunk.graphics.Image;
import com.haxepunk.graphics.Stamp;
import com.haxepunk.HXP;
import com.haxepunk.Scene;
import com.haxepunk.utils.Input;
import com.haxepunk.utils.Key;
import openfl.Assets;
import openfl.display.Bitmap;
import openfl.display.BitmapData;
import psg.Mask;
import psg.Sprite;
import tools.image.Scale3XUpscaler;


/**
 * ...
 * @author Samuel Bouchet
 */

class SpriteGenScene extends Scene
{
	public static var instance:Scene;
	
	private var genSprite:Entity;
	private var genSprite2:Entity;
	private var genSprite3:Entity;
	private var genSprite4:Entity;
	private var genSprite5:Entity;
	private var genSprite6:Entity;
	
	private var planetMask:Mask = new Mask([
      0,0,0,0,0,0,0,0,-1,-1,-1,-1,-1,-1,-1,-1,0,0,0,0,0,0,0,0,
      0,0,0,0,0,0,-1,-1,1,1,1,1,1,1,1,1,-1,-1,0,0,0,0,0,0,
      0,0,0,0,-1,-1,1,1,1,1,1,1,1,1,1,1,1,1,-1,-1,0,0,0,0,
      0,0,0,-1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,-1,0,0,0,
      0,0,-1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,-1,0,0,
      0,0,-1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,-1,0,0,
      0,-1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,-1,0,
      0,-1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,-1,0,
      -1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,-1,
      -1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,-1,
      -1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,-1,
      -1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,-1,
      -1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,-1,
      -1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,-1,
      -1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,-1,
      -1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,-1,
      0,-1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,-1,0,
      0,-1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,-1,0,
      0,0,-1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,-1,0,0,
      0,0,-1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,-1,0,0,
      0,0,0,-1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,-1,0,0,0,
      0,0,0,0,-1,-1,1,1,1,1,1,1,1,1,1,1,1,1,-1,-1,0,0,0,0,
      0,0,0,0,0,0,-1,-1,1,1,1,1,1,1,1,1,-1,-1,0,0,0,0,0,0,
      0,0,0,0,0,0,0,0,-1,-1,-1,-1,-1,-1,-1,-1,0,0,0,0,0,0,0,0
    ], 24, 24, false, false);
	
	private var planet2Mask:Mask = new Mask([
      0,0,0,0,0,0,0,0,-1,-1,-1,-1,-1,-1,-1,-1,0,0,0,0,0,0,0,0,
      0,0,0,0,0,0,-1,-1,2,2,2,2,2,2,2,2,-1,-1,0,0,0,0,0,0,
      0,0,0,0,-1,-1,2,2,2,2,2,2,2,2,2,2,2,2,-1,-1,0,0,0,0,
      0,0,0,-1,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,-1,0,0,0,
      0,0,-1,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,-1,0,0,
      0,0,-1,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,-1,0,0,
      0,-1,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,-1,0,
      0,-1,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,-1,0,
      -1,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,-1,
      -1,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,-1,
      -1,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,-1,
      -1,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,-1,
      -1,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,-1,
      -1,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,-1,
      -1,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,-1,
      -1,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,-1,
      0,-1,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,-1,0,
      0,-1,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,-1,0,
      0,0,-1,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,-1,0,0,
      0,0,-1,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,-1,0,0,
      0,0,0,-1,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,-1,0,0,0,
      0,0,0,0,-1,-1,2,2,2,2,2,2,2,2,2,2,2,2,-1,-1,0,0,0,0,
      0,0,0,0,0,0,-1,-1,2,2,2,2,2,2,2,2,-1,-1,0,0,0,0,0,0,
      0,0,0,0,0,0,0,0,-1,-1,-1,-1,-1,-1,-1,-1,0,0,0,0,0,0,0,0
    ], 24, 24, false, false); 
	
	var robotMask = new Mask([
			0, 0, 0, 0,
			0, 1, 1, 1,
			0, 1, 2, 2,
			0, 0, 1, 2,
			0, 0, 0, 2,
			1, 1, 1, 2,
			0, 1, 1, 2,
			0, 0, 0, 2,
			0, 0, 0, 2,
			0, 1, 2, 2,
			1, 1, 0, 0
	], 4, 11, true, false);
	
	var  humanoidMask = new Mask([
      0, 0, 0, 0, 0, 0, 1, 1,
      0, 0, 0, 0, 0, 1, 2,-1,
      0, 0, 0, 0, 0, 1, 2,-1,
      0, 0, 0, 0, 0, 0, 2,-1,
      0, 0, 0, 0, 1, 1, 2,-1,
      0, 1, 1, 2, 2, 1, 2,-1,
      0, 0, 0, 1, 0, 1, 1, 2,
      0, 0, 0, 0, 1, 1, 1, 2,
      0, 0, 0, 0, 1, 1, 1, 2,
      0, 0, 0, 0, 1, 1, 0, 0,
      0, 0, 0, 1, 1, 1, 0, 0,
      0, 0, 0, 1, 2, 1, 0, 0,
      0, 0, 0, 1, 2, 1, 0, 0,
      0, 0, 0, 1, 2, 2, 0, 0
    ], 8, 14, true, false);
	
	var dragonMask = new Mask([
      0,0,0,0,0,0,0,0,0,0,0,0,
      0,0,0,0,1,1,1,1,0,0,0,0,
      0,0,0,1,1,2,2,1,1,0,0,0,
      0,0,1,1,1,2,2,1,1,1,0,0,
      0,0,0,0,1,1,1,1,1,1,1,0,
      0,0,0,0,0,0,1,1,1,1,1,0,
      0,0,0,0,0,0,1,1,1,1,1,0,
      0,0,0,0,1,1,1,1,1,1,1,0,
      0,0,1,1,1,1,1,1,1,1,0,0,
      0,0,0,1,1,1,1,1,1,0,0,0,
      0,0,0,0,1,1,1,1,0,0,0,0,
      0,0,0,0,0,0,0,0,0,0,0,0
    ], 12, 12, false, false); 

	public function new()
	{
		super();
	
		instance = this;
		genSprite = new Entity();
		genSprite.x = 20;
		genSprite.y = 20;
		
		genSprite2 = new Entity();
		genSprite2.x = 180;
		genSprite2.y = 20;
		
		genSprite3 = new Entity();
		genSprite3.x = 20;
		genSprite3.y = 180;
		
		genSprite4 = new Entity();
		genSprite4.x = 180;
		genSprite4.y = 180;
		
		genSprite5 = new Entity();
		genSprite5.x = 300;
		genSprite5.y = 20;
		
		genSprite6 = new Entity();
		genSprite6.x = 300;
		genSprite6.y = 150;
		
	}

	override public function update()
	{
		super.update();
		
		if (Input.pressed(Key.ESCAPE)) {
			HXP.scene = WelcomeWorld.instance;
		}
		
		
		if (Input.pressed(Key.SPACE)) {
			regenerateAllSprites();
		}
	}
	
	function regenerateSprite(genSprite:Entity, mask:Mask) 
	{
		var newsprite:Sprite = new Sprite(mask, true, 0.45, 0.4, 0.2, 0.6);
		
		var classicScale:Image = new Image( newsprite.bitmap.bitmapData);
		classicScale.scale = 3;
		
		var upscale:Image = new Image( Scale3XUpscaler.upscale(newsprite.bitmap.bitmapData));
		upscale.x = classicScale.scaledWidth + 3;
		
		var graphic:Graphiclist = new Graphiclist([
			classicScale,
			upscale
		]);
		genSprite.graphic = graphic;
	}
	
	function regenerateAllSprites():Void 
	{
		regenerateSprite(genSprite, planet2Mask);
		regenerateSprite(genSprite2, robotMask);
		regenerateSprite(genSprite3, humanoidMask);
		regenerateSprite(genSprite4, dragonMask);
		regenerateSprite(genSprite5, Mask.fromBitmapData(Assets.getBitmapData("gfx/spritegen/test1.png"),false,false));
		regenerateSprite(genSprite6, Mask.fromBitmapData(Assets.getBitmapData("gfx/spritegen/test2.png"),true,true));
	}
	
	override public function render()
	{
		super.render();
	}
	
	override public function begin()
	{
		super.begin();
		regenerateAllSprites();
		add(genSprite);
		add(genSprite2);
		add(genSprite3);
		add(genSprite4);
		add(genSprite5);
		add(genSprite6);
	}
	
	override public function end()
	{
		super.end();
		removeAll();
	}
	
}