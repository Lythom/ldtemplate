package world;
import com.haxepunk.gui.Button;
import com.haxepunk.gui.RadioButton;
import com.haxepunk.HXP;
import com.haxepunk.utils.Input;
import com.haxepunk.utils.Key;
import com.haxepunk.Scene;
import flash.geom.Point;
import flash.Lib;
import tools.grid.AxonometricGridGraphic;
import tools.grid.Cell;
import tools.grid.Grid;
import tools.grid.ICell;

/**
 * ...
 * @author Samuel Bouchet
 */

class BoardGameWorld extends Scene
{
	public static var instance:Scene ;
	
	private var grid:Grid = null;
	private var cursor:Button;

	public function new()
	{
		super();
	
		instance = this;
	}

	override public function update()
	{
		super.update();
		
		if (Input.pressed(Key.ESCAPE)) {
			HXP.scene = WelcomeWorld.instance;
		}
		
		var cell:ICell = grid.getCellAt(new Point(mouseX, mouseY)) ;
		if (cell != null) {
			if (cursor.scene == null) {
				this.add(cursor);
			}
			var p = grid.getCellPosition(cell);
			var size = 18 + Math.round(cell.y/2);
			cursor.width = size;
			cursor.height = size;
			cursor.updateSize();
			
			cursor.x = grid.x + p.x + grid.cellWidth / 2 - cursor.width / 2;
			cursor.y = grid.y + p.y + grid.cellHeight / 2 - cursor.height / 2;
			
		} else if(cursor.scene != null) {
			this.remove(cursor);
		}
	}
	
	override public function render()
	{
		super.render();
	}
	
	override public function begin()
	{
		super.begin();
		if (cursor == null) {
			cursor = new Button(" ",0,0,16,16);
		}
		if (grid == null) {
			grid = new Grid(10, 50, 20, 10);
			grid.addGraphic(new AxonometricGridGraphic(grid,HXP.width - 80, HXP.height - 80, 30));
		}
		this.add(grid);
	}
	
	override public function end()
	{
		super.end();
		this.remove(grid);
	}
	
}