package world;
import com.haxepunk.Entity;
import com.haxepunk.graphics.Image;
import com.haxepunk.HXP;
import com.haxepunk.plateformer.BasicPhysicsEntity;
import com.haxepunk.utils.Draw;
import com.haxepunk.utils.Input;
import com.haxepunk.utils.Key;
import com.haxepunk.Scene;
import flash.display.BitmapData;
import flash.geom.Point;
import plateformer.ControlledHero;
import plateformer.RotatingSword;

/**
 * ...
 * @author Samuel Bouchet
 */

class GameWorld extends Scene
{
	public static var instance:Scene ;
	
	private var ground:Entity;
	private var hero:BasicPhysicsEntity ; 
	private var sword:RotatingSword;

	public function new() 
	{
		super();
		
		instance = this;
	}

	override public function update() 
	{
		super.update();
		
		if (Input.pressed(Key.ESCAPE)) {
			HXP.scene = WelcomeWorld.instance;
		}
		
		if (Input.pressed(Key.NUMPAD_ADD)) {
			sword.distance++;
		}
		
		if (Input.pressed(Key.NUMPAD_SUBTRACT)) {
			sword.distance--;
		}
		
		sword.updatePosition();
	}
	
	override public function begin() 
	{
		hero = new ControlledHero(20, 100);
		
		ground = new Entity(0, 250);
		var img = new Image(new BitmapData(1, 1, false, 0xCDCDCD));
		img.scaleX = 480;
		img.scaleY = 70;
		ground.graphic = img;
		ground.setHitbox(480, 70);
		ground.collidable = true;
		ground.type = "solid";
		add(ground);
		
		ground = new Entity(110, 180);
		var img = new Image(new BitmapData(1, 1, false, 0xAEAEAE));
		img.scaleX = 40;
		img.scaleY = 40;
		ground.graphic = img;
		ground.setHitbox(40, 40);
		ground.collidable = true;
		ground.type = "solid";
		add(ground);
		
		add(hero);
		
		sword = new RotatingSword(hero);
		add(sword);
		
		super.begin();
	}
	
	override public function end()
	{
		super.end();
	}
	
}