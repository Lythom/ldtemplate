package world;
import com.haxepunk.animation.AnimatedSprite;
import com.haxepunk.gui.event.ControlEvent;
import com.haxepunk.gui.FormatAlign;
import com.haxepunk.gui.Label;
import com.haxepunk.gui.MenuItem;
import com.haxepunk.gui.MenuList;
import com.haxepunk.HXP;
import com.haxepunk.normalmap.Light;
import com.haxepunk.Scene;
import com.haxepunk.gui.Button;
import flash.text.TextFormatAlign;
import world.AttributesTestWorld;

/**
 * ...
 * @author Samuel Bouchet
 */

class WelcomeWorld extends Scene
{
	private var bJouer:Button;
	private var bAttributes:Button;
	private var bAnim:Button;
	private var bTrans:Button;
	private var bNormal:Button;
	private var bTest:Button;
	private var bNapeTest:Button;
	private var bRpg:Button;
	private var menu:MenuList;
	
	public static var instance:Scene ;

	public function new()
	{
		super();
		
		var yPos:Int = 20;
		
		var entries:Map<String,Dynamic> = new Map<String,Dynamic>();
		entries.set("1. Jouer", GameWorld);
		entries.set("2. Attributes", AttributesTestWorld);
		//entries.set("3. Animation", AnimationTestWorld);
		entries.set("3. BoardGame", BoardGameWorld);
		entries.set("4. Translation", TranslationTestWorld);
		entries.set("5. Normal Mapping", NormalMappingTestWorld);
		//entries.set("6. Physics engine (Nape)", NapeTestWorld);
		entries.set("6. Sprite Gen", SpriteGenScene);
		entries.set("7. Test", TestWorld);
		entries.set("8. RPG", RpgWorld);
		
		
		menu = new MenuList(150, 20,200);
		var mi:Label;
		var keys = new Array<String>();
		for (k in entries.keys()) {
			keys.push(k);
		}
		keys.sort(function (a:Dynamic, b:Dynamic):Int return a > b ? 1 : (a < b ? -1 : 0));
		
		for (entry in keys) {
			mi = new Label(entry,0,0,200,0, TextFormatAlign.CENTER);
			menu.addControl(mi);
		}
		menu.addEventListener(MenuList.CLICKED, function(e:ControlEvent) {
			HXP.scene = Type.createInstance(entries.get(cast(menu.selectedItem, Label).text), []);
		});

		instance = this;
	}
	
	override public function update()
	{
		super.update();
		menu.update();
	}
	
	override public function begin()
	{
		add(menu);
		
		super.begin();
	}
	
	override public function end()
	{
		removeAll();
		super.end();
	}
	
}